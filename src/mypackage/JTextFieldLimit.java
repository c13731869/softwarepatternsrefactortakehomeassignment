package mypackage;

import javax.swing.*;
import javax.swing.text.*;

class JTextFieldLimit extends PlainDocument {
  private final int LIMIT;
  JTextFieldLimit(int limit) {
    super();
    this.LIMIT = limit;
  }

  public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
    if (str == null)
      return;

    if ((getLength() + str.length()) <= LIMIT)
      super.insertString(offset, str, attr);
    else
      JOptionPane.showMessageDialog(null, "For input " + LIMIT + " characters maximum!");
  }
}