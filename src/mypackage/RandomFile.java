package mypackage;

import java.io.*;
import javax.swing.JOptionPane;

class RandomFile {
	private RandomAccessFile output;
	private RandomAccessFile input;

	void createFile(String fileName) {
		RandomAccessFile file = null;

		try
		{
			file = new RandomAccessFile(fileName, "rw");

		}
		catch (IOException ioException) {
			JOptionPane.showMessageDialog(null, "Error processing file!");
			System.exit(1);
		}

		finally {
            closeRandomAccessFile(file);
		}
	}

	private void closeRandomAccessFile(RandomAccessFile randomAccessFile){
        try {
            if (randomAccessFile != null)
                randomAccessFile.close();
        }
        catch (IOException ioException) {
            JOptionPane.showMessageDialog(null, "Error closing file!");
            System.exit(1);
        }
    }

	void openWriteFile(String fileName) {
		try
		{
			output = new RandomAccessFile(fileName, "rw");
		}
		catch (IOException ioException) {
			JOptionPane.showMessageDialog(null, "File does not exist!");
		}
	}

	void closeWriteFile() {
        closeRandomAccessFile(output);
	}

	long addRecords(Employee newEmployee) {

		long currentRecordStart = 0;

		RandomAccessEmployeeRecord record;

		try
		{
			record = new RandomAccessEmployeeRecord(newEmployee.getEmployeeId(), newEmployee.getPps(),
					newEmployee.getSurname(), newEmployee.getFirstName(), newEmployee.getGender(),
					newEmployee.getDepartment(), newEmployee.getSalary(), newEmployee.getFullTime());

			output.seek(output.length());
			record.write(output);
			currentRecordStart = output.length();
		}
		catch (IOException ioException) {
			JOptionPane.showMessageDialog(null, "Error writing to file!");
		}

		return currentRecordStart - RandomAccessEmployeeRecord.SIZE;
	}

	void changeRecords(Employee oldDetails, long currentRecordStart) {

		RandomAccessEmployeeRecord record;
		try
		{
			record = new RandomAccessEmployeeRecord(oldDetails.getEmployeeId(), oldDetails.getPps(),
					oldDetails.getSurname(), oldDetails.getFirstName(), oldDetails.getGender(),
					oldDetails.getDepartment(), oldDetails.getSalary(), oldDetails.getFullTime());

			output.seek(currentRecordStart);
			record.write(output);
		}
		catch (IOException ioException) {
			JOptionPane.showMessageDialog(null, "Error writing to file!");
		}
	}

	void deleteRecords(long currentRecordStart) {

		RandomAccessEmployeeRecord record;


		try
		{
			record = new RandomAccessEmployeeRecord();
			output.seek(currentRecordStart);
			record.write(output);
		}
		catch (IOException ioException) {
			JOptionPane.showMessageDialog(null, "Error writing to file!");
		}
	}

	void openReadFile(String fileName) {
		try
		{
			input = new RandomAccessFile(fileName, "r");
		}
		catch (IOException ioException) {
			JOptionPane.showMessageDialog(null, "File is not suported!");
		}
	}

	void closeReadFile() {
        closeRandomAccessFile(input);
    }

	long getFirst() {
		long byteToStart = 0;

		try {
			input.length();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		return byteToStart;
	}

	long getLast() {
		long byteToStart = 0;

		try {
			byteToStart = input.length() - RandomAccessEmployeeRecord.SIZE;
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		return byteToStart;
	}

	long getNext(long readFrom) {
		long byteToStart = readFrom;

		try {
			input.seek(byteToStart);

			if (byteToStart + RandomAccessEmployeeRecord.SIZE == input.length())
				byteToStart = 0;
			else
				byteToStart = byteToStart + RandomAccessEmployeeRecord.SIZE;
		}
		catch (NumberFormatException |IOException e) {
			e.printStackTrace();
		}

		return byteToStart;
	}

	long getPrevious(long readFrom) {
		long byteToStart = readFrom;

		try {
			input.seek(byteToStart);

			if (byteToStart == 0)
				byteToStart = input.length() - RandomAccessEmployeeRecord.SIZE;
			else
				byteToStart = byteToStart - RandomAccessEmployeeRecord.SIZE;
		}
		catch (NumberFormatException |IOException e) {
            e.printStackTrace();
		}
		return byteToStart;
	}

	Employee readRecords(long byteToStart) {
		Employee thisEmp;
		RandomAccessEmployeeRecord record = new RandomAccessEmployeeRecord();

		try {
			input.seek(byteToStart);
			record.read(input);
		}
		catch (IOException e) {
            e.printStackTrace();
		}

		thisEmp = record;

		return thisEmp;
	}

	boolean isPpsExist(String pps, long oldByteStart) {
		RandomAccessEmployeeRecord record = new RandomAccessEmployeeRecord();
		boolean ppsExist = false;
		long currentByte = 0;

		try {
			while (currentByte != input.length() && !ppsExist) {

				if (currentByte != oldByteStart) {
					input.seek(currentByte);
					record.read(input);

					if (record.getPps().trim().equalsIgnoreCase(pps)) {
						ppsExist = true;
						JOptionPane.showMessageDialog(null, "PPS number already exist!");
					}
				}
				currentByte = currentByte + RandomAccessEmployeeRecord.SIZE;
			}
		}
		catch (IOException e) {
            e.printStackTrace();
		}

		return ppsExist;
	}

	boolean isSomeoneToDisplay() {
		boolean someoneToDisplay = false;
		long currentByte = 0;
		RandomAccessEmployeeRecord record = new RandomAccessEmployeeRecord();

		try {
			while (currentByte != input.length() && !someoneToDisplay) {
				input.seek(currentByte);
				record.read(input);
				if (record.getEmployeeId() > 0)
					someoneToDisplay = true;
				currentByte = currentByte + RandomAccessEmployeeRecord.SIZE;
			}
		}
		catch (IOException e) {
            e.printStackTrace();
		}

		return someoneToDisplay;
	}
}
