package mypackage;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

class SearchBySurnameDialog extends JDialog implements ActionListener{
	private final EmployeeDetails PARENT;
	private JButton search, cancel;
	private JTextField searchField;

	SearchBySurnameDialog(EmployeeDetails parent) {
		setTitle("Search by Surname now");
		setModal(true);
		this.PARENT = parent;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JScrollPane scrollPane = new JScrollPane(searchPane());
		setContentPane(scrollPane);

		getRootPane().setDefaultButton(search);

		setSize(500, 190);
		setLocation(350, 250);
		setVisible(true);
	}

	private Container searchPane() {
		JPanel searchPanel = new JPanel(new GridLayout(3,1));
		JPanel textPanel = new JPanel();
		JPanel buttonPanel = new JPanel();
		JLabel searchLabel;

		searchPanel.add(new JLabel("Search by Surname"));

		textPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		textPanel.add(searchLabel = new JLabel("Enter Surname:"));
		searchLabel.setFont(this.PARENT.FONT1);
		textPanel.add(searchField = new JTextField(20));
		searchField.setFont(this.PARENT.FONT1);
		searchField.setDocument(new JTextFieldLimit(20));

		buttonPanel.add(search = new JButton("Search"));
		search.addActionListener(this);
		search.requestFocus();

		buttonPanel.add(cancel = new JButton("Cancel"));
		cancel.addActionListener(this);

		searchPanel.add(textPanel);
		searchPanel.add(buttonPanel);

		return searchPanel;
	}

	public void actionPerformed(ActionEvent e) {

		if(e.getSource() == search){
			this.PARENT.searchBySurnameField.setText(searchField.getText());

			this.PARENT.searchEmployeeBySurname();
			dispose();
		}
		else if(e.getSource() == cancel)
			dispose();
	}
}
